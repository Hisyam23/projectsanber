@extends('layout.master')
@section('judul')
  Hallo {{ Auth::user()->name }}
@endsection

@section('isi')

<form>
  <div class="form-group">
    <label>Question</label>
    <textarea class="form-control" rows="3"></textarea>
  </div>

  <div class="col-3 mb-3">
     
      <form action="#" method='POST'>
                    @csrf
                    @method('delete')
                    <a href="/answer" type="submit" class="btn btn-primary btn-sm">Komentar</a>
                    <a href="" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
    </div>
    
  
  
</form>



@endsection